<?php

require_once 'vendor/autoload.php';

use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\ListBlobsOptions;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;

$aName = "ridhowebapp";
$aKey = "OA1JVv23wkfVh3xfoZT7jegJZnbrCfI8hkcaGpusEecbcuiJY/qw+0X2NtJEc9wqrjfyg/zaIOzjuBH/x58Bfg==";

$connectionString = "DefaultEndpointsProtocol=https;AccountName=" . $aName . ";AccountKey=" . $aKey;

// Create blob client.
$blobClient = BlobRestProxy::createBlobService($connectionString);

// ambil data file
$namaFile = $_FILES['berkas']['name'];
$namaSementara = $_FILES['berkas']['tmp_name'];

$createContainerOptions = new CreateContainerOptions();

$createContainerOptions->setPublicAccess(PublicAccessType::CONTAINER_AND_BLOBS);

// Menetapkan metadata dari container.
$createContainerOptions->addMetaData("key1", "value1");
$createContainerOptions->addMetaData("key2", "value2");

$containerName = "gambar" . uniqid();

$fileToUpload = $namaFile;

try {
    // Membuat container.

    $blobClient->createContainer($containerName, $createContainerOptions);

    $upFile = fopen($namaSementara, "r");

    //Upload blob
    $blobClient->createBlockBlob($containerName, $fileToUpload, $upFile);

    // List blobs.
    $listBlobsOptions = new ListBlobsOptions();

    do {
        $result = $blobClient->listBlobs($containerName, $listBlobsOptions);
        foreach ($result->getBlobs() as $blob) {
            echo $blob->getUrl();
        }

        $listBlobsOptions->setContinuationToken($result->getContinuationToken());
    } while ($result->getContinuationToken());
} catch (ServiceException $e) {
    // Handle exception based on error codes and messages.
    // Error codes and messages are here:
    // http://msdn.microsoft.com/library/azure/dd179439.aspx
    $code = $e->getCode();
    $error_message = $e->getMessage();
    echo $code . ": " . $error_message . "<br />";
}
