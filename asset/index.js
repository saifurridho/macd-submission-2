$(document).ready(function () {

    $('#berkas').on('change', function () {
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        var nama = fileName.substring(12);
        nama = nama.substr(0, 20) + "...";
        $(this).next('.custom-file-label').html(nama);
    })

    $('.progup').hide();
    $('.progan').hide();
    $('#gambar').hide();
    $('.imgcap').hide();

    $("#btnSubmit").click(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();
        var fd = new FormData();
        var files = $('#berkas')[0].files[0];
        fd.append('berkas', files);

        // disabled the submit button
        $("#btnSubmit").prop("disabled", true);
        $("#btnSubmit").val("Mengupload Gambar...")
        $(".progup").show();

        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "upload.php",
            data: fd,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 800000,
            success: function (data) {

                $('#gambar').attr("src", data);
                $("#gambar").show();
                $(".progup").hide();
                processImage(data);

            },
            error: function (e) {

                $("#output").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);

            }
        });

    });

});

function processImage(urlgambar) {

    $(".progan").show();
    $("#btnSubmit").val("Menganalisa Gambar...")
    // **********************************************
    // *** Update or verify the following values. ***
    // **********************************************

    // Replace <Subscription Key> with your valid subscription key.
    var subscriptionKey = "e8c4f4d3aeba426cb55ba473689cc4f7";

    // You must use the same Azure region in your REST API method as you used to
    // get your subscription keys. For example, if you got your subscription keys
    // from the West US region, replace "westcentralus" in the URL
    // below with "westus".
    //
    // Free trial subscription keys are generated in the "westus" region.
    // If you use a free trial subscription key, you shouldn't need to change
    // this region.
    var uriBase =
        "https://southeastasia.api.cognitive.microsoft.com/vision/v2.0/analyze";

    // Request parameters.
    var params = {
        "visualFeatures": "Categories,Description,Color",
        "details": "",
        "language": "en",
    };

    // Make the REST API call.
    $.ajax({
            url: uriBase + "?" + $.param(params),

            // Request headers.
            beforeSend: function (xhrObj) {
                xhrObj.setRequestHeader("Content-Type", "application/json");
                xhrObj.setRequestHeader(
                    "Ocp-Apim-Subscription-Key", subscriptionKey);
            },

            type: "POST",

            // Request body.
            data: '{"url": ' + '"' + urlgambar + '"}',
        })

        .done(function (data) {
            $("#btnSubmit").val('Analisa');
            $("#btnSubmit").prop("disabled", false);
            $('.progan').hide();
            // Show formatted JSON on webpage.
            var hasil = JSON.stringify(data, null, 2)
            var hasil2 = JSON.parse(hasil);
            var caption = hasil2.description.captions[0].text;
            $(".imgcap").show();
            $(".imgcap").text(caption);
        })

        .fail(function (jqXHR, textStatus, errorThrown) {
            // Display error message.
            var errorString = (errorThrown === "") ? "Error. " :
                errorThrown + " (" + jqXHR.status + "): ";
            errorString += (jqXHR.responseText === "") ? "" :
                jQuery.parseJSON(jqXHR.responseText).message;
            alert(errorString);
            $('.progup').hide();
            $('.progan').hide();
            $('#gambar').hide();
            $('.imgcap').hide();
            $("#btnSubmit").val('Analisa');
            $("#btnSubmit").prop("disabled", false);
        });
};